import React, { Component, Fragment } from 'react'
import Game from './screens/Game'
import Login from './screens/Login'
import { openGame as sendGame } from './services'

export default class App extends Component {
  state = {
    inGame: false,
    name: '',
    player: '',
    gameFetched: {}
  }

  toggleGame = () =>
    this.setState(prev => ({
      inGame: !prev.inGame
    }))

  setRoom = name => this.setState({ name })
  setUser = player => this.setState({ player })

  openGame = () => {
    let { name, player } = this.state

    sendGame(name, player).then(res => {
      console.log(res)
      if (res.status) {
        this.setState({ gameFetched: res.row.state }, this.toggleGame)
      }
    })
  }

  render() {
    return (
      <Fragment>
        {this.state.inGame ? (
          <Game
            logged={this.state.player}
            name={this.state.name}
            users={this.state.users}
            trigger={this.toggleGame}
            initialGameData={this.state.gameFetched}
          />
        ) : (
          <Login
            startGame={this.openGame}
            setPlayer={this.setUser}
            setRoom={this.setRoom}
          />
        )}
      </Fragment>
    )
  }
}
