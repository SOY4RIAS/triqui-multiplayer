const URL = `http://192.168.1.136:3000/`

export const openGame = async (name, player) => {
	let sendObj = {
		name,
		player,
	}

	sendObj = JSON.stringify(sendObj)

	let res = await fetch(`${URL}`, {
		method  : 'POST',
		body    : sendObj,
		headers : {
			'Content-Type' : 'application/json',
		},
	})

	res = await res.json()
	return res
}

export const saveGameData = async (state, name) => {
	let res = await fetch(`${URL}`, {
		method  : 'PUT',
		body    : JSON.stringify({ state, name }),
		headers : {
			'Content-Type' : 'application/json',
		},
	})

	res = await res.json()

	return res
}

export const getGame = async name => {
	let res = await fetch(`${URL}${name}`)

	res = res.json()

	return res
}
