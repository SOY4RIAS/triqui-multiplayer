import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  Text,
  SafeAreaView,
  TouchableOpacity
} from 'react-native'
import styles from './Game.style'
import { saveGameData, getGame } from '../services'

class Game extends Component {
  state = {
    player1: 0,
    player2: 0,
    users: {
      player1: '',
      player2: ''
    },
    won: 0,
    boxes: [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
    turn: 1,
    reset: false
  }

  evaluate = async (row, column) => {
    let { boxes, turn, won, users } = this.state
    if (boxes[row][column] != 0) return

    if (this.props.logged === users.player1) {
      if (turn != 1) {
        console.log(turn)
        return
      }
    } else if (this.props.logged === users.player2) {
      if (turn != 2) {
        console.log(turn)
        return
      }
    }

    if (won != 0) return

    boxes[row][column] = turn
    let newTurn = turn == 1 ? 2 : 1
    this.setState({ boxes, turn: newTurn }, this.saveState)

    let rowV = this.state.boxes[row],
      columnV = this.state.boxes.map(i => i[column]),
      vert1 = [boxes[0][0], boxes[1][1], boxes[2][2]],
      vert2 = [boxes[2][0], boxes[1][1], boxes[0][2]]

    if (
      this.equal(columnV) ||
      this.equal(vert1) ||
      this.equal(vert2) ||
      this.equal(rowV)
    ) {
      await this.setState({ won: turn }, this.saveState)
      if (this.state.won == 1) {
        this.setState(
          prev => ({
            player1: prev.player1 + 1
          }),
          this.saveState
        )
      } else {
        this.setState(
          prev => ({
            player2: prev.player2 + 1
          }),
          this.saveState
        )
      }
      alert(
        `El ganador es ${
          this.state.won == 1 ? this.state.users.player1 : this.state.users.player2
        }`
      )
    }

    if (this.nonEmpty(boxes.flat())) {
      alert('Empate')
      this.restart()
      return
    }
  }

  restart = () =>
    this.setState(
      {
        won: 0,
        boxes: [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
      },
      this.saveState
    )

  equal = (arr = []) => arr.every(v => v === arr[0] && v !== 0)
  nonEmpty = (arr = []) => arr.every(v => v != 0)

  saveState = () => {
    let { state, props } = this
    saveGameData(state, props.name).then(res => console.log(res, 'SETTER'))
  }

  getter = () => {
    let { props } = this
    setInterval(() => {
      getGame(props.name).then(res => {
        if (res.status) {
          let { state } = res.row

          console.log(state, 'STATE')

          this.setState({ ...state }, () => console.log(this.state))
        }
      })
    }, 2000)
  }

  componentWillMount() {
    let { initialGameData } = this.props
    console.log(initialGameData)
    this.setState({ ...initialGameData })
  }

  componentDidMount() {
    this.getter()
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.scorePanel}>
          <Text> {this.props.name} </Text>
          <View style={styles.scoreNames}>
            <View style={styles.rightScore}>
              <Text style={styles.textScore}>{this.state.users.player1}</Text>
            </View>
            <View style={styles.leftScore}>
              <Text style={styles.textScore}>{this.state.users.player2}</Text>
            </View>
          </View>
          <View style={styles.scoreNames}>
            <View style={styles.rightScore}>
              <Text style={styles.textScore}>{this.state.player1}</Text>
            </View>
            <View style={styles.leftScore}>
              <Text style={styles.textScore}>{this.state.player2}</Text>
            </View>
          </View>
          <View style={styles.restartContainer}>
            <TouchableOpacity onPress={this.props.trigger}>
              <Text> Cerrar </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.restart}>
              <Text> Reiniciar </Text>
            </TouchableOpacity>
          </View>
        </View>
        {this.state.boxes.map((row, i) => (
          <View key={i} style={styles.subContainer}>
            {row.map((column, j) => (
              <TouchableOpacity
                key={j}
                style={styles.tileContainer}
                onPress={() => this.evaluate(i, j)}
              >
                <View style={styles.contentContainer}>
                  <Text style={styles.textTile}>
                    {column != 0 && (column == 1 ? 'X' : 'O')}
                  </Text>
                </View>
              </TouchableOpacity>
            ))}
          </View>
        ))}
      </SafeAreaView>
    )
  }
}

export default Game
