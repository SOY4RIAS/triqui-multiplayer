import React from 'react'
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TextInput,
  TouchableOpacity
} from 'react-native'

const Login = ({ startGame, setPlayer, setRoom }) => (
  <SafeAreaView style={styles.container}>
    <View>
      <TextInput
        style={styles.textInput}
        placeholder='ROOM NAME'
        placeholderTextColor='#eaeaea'
        onChangeText={setRoom}
      />
      <TextInput
        style={styles.textInput}
        placeholder='PLAYER NAME'
        placeholderTextColor='#eaeaea'
        onChangeText={setPlayer}
      />
      <TouchableOpacity onPress={startGame}>
        <View style={styles.bottonLogin}>
          <Text style={{ textAlign: 'center', color: 'white' }}>Start Game</Text>
        </View>
      </TouchableOpacity>
    </View>
  </SafeAreaView>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'gray',
    justifyContent: 'center',
    alignItems: 'center'
  },
  textInput: {
    borderWidth: 0.5,
    width: 200,
    height: 50,
    borderRadius: 4,
    borderColor: 'transparent',
    borderBottomColor: '#000000',
    marginBottom: 10,
    padding: 10
  },
  bottonLogin: {
    borderRadius: 10,
    backgroundColor: 'black',
    padding: 10
  }
})

export default Login
