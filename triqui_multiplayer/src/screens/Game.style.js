import { StyleSheet } from 'react-native'
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'gray',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  subContainer: {
    flex: 1,
    marginTop: '20%',
    flexDirection: 'column'
  },
  tileContainer: {
    borderRadius: 10,
    margin: 10,
    height: 100,
    backgroundColor: 'black',
    borderWidth: 1,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1
  },
  contentContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textTile: {
    color: 'white',
    fontSize: 60,
    fontWeight: '800'
  },
  scorePanel: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    margin: 30,
    borderRadius: 10,
    position: 'absolute',
    top: 20,
    left: 0,
    right: 0
  },
  rightScore: {
    flex: 1,
    alignSelf: 'center'
  },
  leftScore: {
    padding: 10,
    flex: 1,
    alignSelf: 'center'
  },
  textScore: {
    textAlign: 'center',
    color: 'black',
    fontSize: 19
  },
  scoreNames: {
    flexDirection: 'row-reverse',
    alignItems: 'center'
  },
  restartContainer: {
    padding: 10,
    flexDirection: 'row-reverse',
    justifyContent: 'center'
  }
})
