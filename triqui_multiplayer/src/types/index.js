type StateGame = {
  player1: number,
  player2: number,
  won: number,
  boxes: Array<Array<number>>,
  turn: number,
  reset: boolean
}

type PropsGame = {
  trigger: func,
  users: Object
}

export { StateGame, PropsGame }
