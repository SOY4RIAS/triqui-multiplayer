const { Schema, model } = require('mongoose')

const Game = Schema({
  name: {
    type: String,
    unique: true,
    required: [true, 'Nombre de la sala es obligatorio']
  },
  state: {
    type: Object,
    required: true
  }
})

module.exports = model('Game', Game)
