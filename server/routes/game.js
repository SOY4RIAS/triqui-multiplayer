const express = require('express')
const Game = require('./../models/Game')
const app = express()

app.get('/:name?', async (req, res) => {
  let { name } = req.params

  let row = await Game.findOne({ name })

  console.log(row, 'fetched')

  res.json({ status: true, row })
})

app.post('/', async (req, res) => {
  let { name, player } = req.body

  let existing = await Game.findOne({ name })

  if (!existing) {
    console.log('in')
    game = new Game({
      name,
      state: {
        player1: 0,
        player2: 0,
        users: {
          player1: player,
          player2: ''
        },
        won: 0,
        boxes: [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
        turn: 1,
        reset: false
      }
    })

    return game
      .save()
      .then(row => res.json({ status: true, row }))
      .catch(err => res.json({ status: false }))
  } else {
    let { state } = existing
    state.users.player2 = player
    Game.findByIdAndUpdate(
      existing._id,
      {
        state
      },
      { new: true }
    ).then(row => {
      console.log(row, 'Opened')
      res.json({ status: true, row })
    })
  }
})

app.put('/', (req, res) => {
  let { name, state } = req.body
  Game.findOneAndUpdate({ name }, { state })
    .then(doc => res.json({ status: true, doc }))
})

module.exports = app
